import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

ListView {

    model: ListModel {
        ListElement {
            label: "Element 1"
            section: 1
        }
        ListElement {
            label: "Element 2"
            section: 1
        }
        ListElement {
            label: "Element 4"
            section: 2
        }
        ListElement {
            label: "Element 5"
            section: 2
        }
        ListElement {
            label: "Element 6"
            section: 2
        }
    }



    delegate: Kirigami.BasicListItem {
        text: model.label
        icon: "document-share"
    }

    section.property: "section"
    section.delegate: Kirigami.ListSectionHeader {
        text: "Section " + section
    }



}
