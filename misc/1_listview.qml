import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

ListView {

    model: 20

    delegate: Kirigami.BasicListItem {
        text: "Element " + modelData
        icon: "document-share"
    }
}
