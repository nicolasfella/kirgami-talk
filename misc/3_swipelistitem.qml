import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

ListView {

    model: 4

    delegate: Kirigami.SwipeListItem {
        Label {
            text: "Item " + modelData
        }

        actions: [
            Kirigami.Action {
                text: "Delete"
                iconName: "delete"
            },
            Kirigami.Action {
                text: "Info"
                iconName: "documentinfo"
            }
        ]
    }
}
