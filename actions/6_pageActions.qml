import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

Kirigami.ApplicationWindow {

    pageStack.initialPage: mainPage

    Component {
        id: mainPage
        Kirigami.Page {
            title: "Main page"

            Label {
                id: theLabel
                text: "Some page"
                anchors.centerIn: parent
            }

            actions.main: Kirigami.Action {
                text: "Edit"
                iconName: "document-edit"
                onTriggered: theLabel.text = "new Text"
            }
            actions.left: Kirigami.Action {
                text: "Delete"
                iconName: "delete"
            }
            actions.right: Kirigami.Action {
                text: "Share"
                iconName: "document-share"
            }
        }
    }
}
