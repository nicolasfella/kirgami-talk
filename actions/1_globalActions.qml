import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

Kirigami.ApplicationWindow {

    pageStack.initialPage: mainPage

    globalDrawer: Kirigami.GlobalDrawer {

        actions: [
            Kirigami.Action {
                text: "Open"
                iconName: "document-open"
                onTriggered: {console.log("Opening things")}
            },
            Kirigami.Action {
                text: "Save"
                iconName: "document-save"
                onTriggered: {console.log("Saving things")}
            }
        ]
    }

    Component {
        id: mainPage
        Kirigami.Page {
            title: "Main page"

            Label {
                text: "Some page"
                anchors.centerIn: parent
            }
        }
    }
}
