import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

Kirigami.ApplicationWindow {

    pageStack.initialPage: mainPage

    Component {
        id: mainPage
        Kirigami.Page {
            title: "Main page"

            Label {
                text: "Hello World"
                anchors.centerIn: parent
            }

        }
    }


}
