import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

Kirigami.Page {
    title: "Second page"

    Column {
        anchors.centerIn: parent

        Label {
            text: "Another page"
        }

        Button {
            text: "Close"
            onClicked: pageStack.pop()
        }
    }
}
