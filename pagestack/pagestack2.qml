import QtQuick 2.10
import QtQuick.Controls 2.10

import org.kde.kirigami 2.10 as Kirigami

Kirigami.ApplicationWindow {

    pageStack.initialPage: mainPage

    Component {
        id: mainPage
        Kirigami.Page {
            title: "Main page"

            Button {
                text: "Open another page"
                anchors.centerIn: parent
                onClicked: pageStack.push(secondPage)
            }

        }
    }

    Component {
        id: secondPage
        Kirigami.Page {
            title: "Second page"

            Column {
                anchors.centerIn: parent

                Label {
                    text: "Another page"
                }

                Button {
                    text: "Close"
                    onClicked: pageStack.pop()
                }
            }
        }
    }
}
